#include "ofxAudioIO.h"

void ofxAudioIO::setup() {
    // Setup GUI
    gui.setup(guiName);
    gui.add(listDevicesButton.setup("List Devices"));
    gui.add(inputDeviceID.setup("Input Device ID", 0, 0, 10));
    gui.add(outputDeviceID.setup("Output Device ID", 0, 0, 10));

    listDevicesButton.addListener(this, &ofxAudioIO::listDevices);

    setupAudio();
}

void ofxAudioIO::setup(string name) {
    guiName=name;
    // Setup GUI with name
    setup();
}

void ofxAudioIO::draw() {
    drawOscilloscope(); // Add this line
    gui.draw();
}

void ofxAudioIO::drawOscilloscope() {
    ofPushStyle();
    ofNoFill();
    ofBeginShape();
    for (size_t i = 0; i < leftChannel.size(); ++i) {
        // Map the audio signal to the window size
        float x = ofMap(leftChannel[i], -1, 1, 0, ofGetWidth());
        float y = ofMap(rightChannel[i], -1, 1, 0, ofGetHeight());
        ofVertex(x, y);
    }
    ofEndShape();
    ofPopStyle();
}

void ofxAudioIO::listDevices() {
    auto devices = soundStream.getDeviceList();
    for (auto &device : devices) {
        ofLogNotice() << "Device: " << device.name;
    }
}

void ofxAudioIO::audioIn(ofSoundBuffer &input) {
    // Process input buffer
    // For example, copy data to class members, perform analysis, etc.
}   
void ofxAudioIO::setupAudio() {
    // Setup sound stream based on GUI selection
    ofSoundStreamSettings settings;
    settings.setInDevice(soundStream.getDeviceList()[inputDeviceID]);
    settings.setOutDevice(soundStream.getDeviceList()[outputDeviceID]);
    // Further sound stream setup...
    settings.setInListener(this); // Set this object as the listener for audio input

    soundStream.setup(settings);
}
