#pragma once
#include "ofMain.h"
#include "ofxGui.h"

class ofxAudioIO {
public:
    void setup();
    void setup(string name);
    void draw();
    ofxGuiGroup gui;
    void audioIn(ofSoundBuffer &input); 

private:
    ofSoundStream soundStream;
    ofxButton listDevicesButton;
    ofxIntSlider inputDeviceID;
    ofxIntSlider outputDeviceID;
    ofParameter <string> guiName = ofToString("ofxAudioIO");

    void listDevices();
    void setupAudio();

    vector<float> leftChannel;
    vector<float> rightChannel;

    void drawOscilloscope();
};
